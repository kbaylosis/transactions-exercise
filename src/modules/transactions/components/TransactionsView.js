import React, { PureComponent } from "react";
import { Button, Col, Divider, Row, Switch, Table } from 'antd';
import PropTypes from "prop-types";
import styled from '@emotion/styled';
import 'antd/dist/antd.css';
import { RetweetOutlined } from '@ant-design/icons';

const Content = styled.div`
  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.08);
  padding: 10px;
`;

const locale = "en-us";

const columns = [
    {
        title: 'Timestamp',
        dataIndex: 'date',
        key: 'date',
    },
    {
        title: 'Transaction ID',
        dataIndex: 'id',
        key: 'id',
    },
    {
        title: 'Card ID',
        dataIndex: 'cardId',
        key: 'cardId',
    },
    {
        title: 'Amount',
        dataIndex: 'amount',
        key: 'amount',
    },
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
    },
];

class TransactionsView extends PureComponent {
    componentDidMount() {
		this.props.actions.fetchData();
	}

    render() {
        const { actions: { fetchData, toggleCurrency }, currency, error, loading,
            total } = this.props;
        
        return (
            <Content>
                <Row justify="end">
                    <Col flex="auto">{ error ? "Error loading the data!" : ""}</Col>
                    <Col flex="160px" justify="end">
                        <Switch checkedChildren="USD" unCheckedChildren="CAD"
                            defaultChecked onClick={toggleCurrency} loading={loading}/>
                        <Divider type="vertical"/>
                        <Button shape="round" icon={<RetweetOutlined />} size="small"
                            onClick={fetchData}/>
                    </Col>
                </Row>
                <Divider/>
                {this._listRecords()}
                <Divider/>
                <Row>
                    <Col flex="auto"/>
                    <Col flex="160px" justify="end">
                    {
                        `TOTAL: ${
                        Intl.NumberFormat(locale, {
                            style: "currency",
                            currency: currency,
                        }).format(this._convert(total))
                        }`
                    }
                    </Col>
                </Row>
            </Content>
        );
    }

    _listRecords = () => {
        const { currency, error, loading, records } = this.props;

        var key = 1;
        const dataSource = error ? [] : ((records && records.length > 0) ? records.map((r) => 
             ({
                key: key++,
                id: r.id,
                date: Intl.DateTimeFormat(locale, {
                    year: 'numeric', month: 'numeric', day: 'numeric',
                    hour: 'numeric', minute: 'numeric', second: 'numeric',
                }).format(r.date),
                amount: Intl.NumberFormat(locale, {
                    style: "currency",
                    currency: currency,
                }).format(this._convert(r.amountInUSDCents)),
                cardId: r.cardId,
                name: `${r.user.firstName} ${r.user.lastName}`,
            })
        ) : []);

        return <Table dataSource={dataSource} columns={columns} size="small" loading={loading} />;
    }

    _convert = (amount) => {
        const { currency, rate } = this.props;

        return (currency === 'USD' ? amount : amount * rate) / 100;
    }
}

TransactionsView.propTypes = {
    // Attributes
    currency: PropTypes.string.isRequired,
    records: PropTypes.arrayOf(PropTypes.shape({
		id: PropTypes.string.isRequired,
        amountInUSDCents: PropTypes.number.isRequired,
        date: PropTypes.any.isRequired,
        merchantNetworkId: PropTypes.string.isRequired,
        cardId: PropTypes.string.isRequired,
        merchant: PropTypes.shape({
            currency: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired,
            networkId: PropTypes.string.isRequired,
        }),
        user: PropTypes.shape({
            id: PropTypes.number.isRequired,
            firstName: PropTypes.string.isRequired,
            lastName: PropTypes.string.isRequired,
            cardId: PropTypes.string.isRequired,
        }),
	})),
    error: PropTypes.string,
    loading: PropTypes.bool.isRequired,
    total: PropTypes.number.isRequired,

    // Functions
    actions: PropTypes.shape({
        toggleCurrency: PropTypes.func.isRequired,
        fetchData: PropTypes.func.isRequired,
    }).isRequired,
};

export default TransactionsView;