export const DATA_FETCHING = "transactions/types/DATA_FETCHING";
export const DATA_FETCHED = "transactions/types/DATA_FETCHED";
export const DATA_FETCH_FAILED = "transactions/types/DATA_FETCH_FAILED";
export const TOGGLE_CURRENCY = "transactions/types/TOGGLE_CURRENCY";