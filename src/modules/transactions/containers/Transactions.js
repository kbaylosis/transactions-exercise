import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import * as ActionsCreators from "../actions";
import TransactionsView from "../components/TransactionsView";

const mapStateToProps = ({ transactions }) => ({
	...transactions,
});

const mapDispatchToProps = (dispatch) => ({
	actions: bindActionCreators(ActionsCreators, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(TransactionsView);
