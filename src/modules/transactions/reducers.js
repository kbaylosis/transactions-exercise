import { combineReducers } from "redux";

import * as Types from "./types";

const currency = (state = 'USD', action) => {
	switch (action.type) {
	case Types.TOGGLE_CURRENCY:
        return state === 'USD' ? 'CAD' : 'USD';
	default:
		return state;
	}
};

const error = (state = null, action) => {
    switch (action.type) {
        case Types.DATA_FETCH_FAILED:
            return action.error;
        case Types.DATA_FETCHING:
        case Types.DATA_FETCHED:
            return null;
        default:
            return state;
    }
}

const loading = (state = false, action) => {
    switch (action.type) {
        case Types.DATA_FETCHING:
            return true;
        case Types.DATA_FETCH_FAILED:
        case Types.DATA_FETCHED:
            return false;
        default:
            return state;
    }
}

const records = (state = [], action) => {
	switch (action.type) {
    case Types.DATA_FETCH_FAILED:
        return [];
    case Types.DATA_FETCHED:
        return action.data;
    case Types.DATA_FETCHING:
	default:
		return state;
	}
};

const rate = (state = 0.0, action) => {
	switch (action.type) {
    case Types.DATA_FETCHED:
        return 1.21;
    case Types.DATA_FETCH_FAILED:
    case Types.DATA_FETCHING:
	default:
		return state;
	}
};

const total = (state = 0.0, action) => {
	switch (action.type) {
    case Types.DATA_FETCH_FAILED:
        return 0.0;
    case Types.DATA_FETCHED:
        return action.total;
    case Types.DATA_FETCHING:
	default:
		return state;
	}
};


export default combineReducers({
	currency,
    error,
    loading,
    rate,
    records,
    total,
});