import _ from "lodash";

import * as Types from "./types";
import {getMerchants, getTransactions, getUsers} from '../../utils/GraphQLData';

export const toggleCurrency = () => ({
    type: Types.TOGGLE_CURRENCY,
});

export const fetchData = () => (
    async (dispatch) => {
        try {
            dispatch({ type: Types.DATA_FETCHING });
            
            const merchants = await getMerchants();
            const transactions = await getTransactions();
            const users = await getUsers();

            var total = 0;
            const mappedData = _.transform(transactions, (result, n) => {
                const value = {
                    ...n,
                    user: _.find(users, (o) => o.cardId === n.cardId),
                    merchant: _.find(merchants, (o) => o.networkId === n.merchantNetworkId),
                };

                result.splice(_.sortedIndexBy(result, value, (o) => -o.date), 0, value);

                total += value.amountInUSDCents;
            });

            dispatch({ type: Types.DATA_FETCHED, data: mappedData, total: total}); 
        } catch (error) {
            dispatch({ type: Types.DATA_FETCH_FAILED, data: [], error: 'error_loading_data' });
        }
    }
);