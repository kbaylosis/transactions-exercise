import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";

import AppReducer from "./reducers";
import routes from "./routes";

const store = createStore(
	AppReducer,
	applyMiddleware(thunk)
);

function App() {
  return (
    <Provider store={ store }>
      {routes[0]}
    </Provider>
  );
}

export default App;
